# Devianze Block-list

- [__blocklist.csv__](./blocklist.csv): Baseline block-list for devianze.city instance.
- [__unified.csv__](./unified.csv): Actual live block-list, periodically synchronized with peers according to [conf.toml](./conf.toml).

## Usage

### Install dependencies

Install CLI dependencies into a Python environment:

```
pip install -r requirements.txt
```

### Configure fediblock-hole

First, create a `conf.toml` file, starting from `sample.conf.toml`, by compiling it with a proper admin token with the `admin:read:domain_blocks` permission.


### Sync block-list

Sync with external sources, without pushing immediately to the destination servers.

```
fediblock-sync -c conf.toml --no-push-instance
```

Inspect the changes:

```
git diff -U0 unified.csv
```

### Push block-list to server

To push the new unified block-list to the server, and make them effective, run `fediblock-sync` without the `--no-push-instance`.

```
fediblock-sync -c conf.toml
```